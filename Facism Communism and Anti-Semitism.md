# Facism Communism and Anti-Semitism (Pages 210 - 211)

**1. What was the aim of Communism and why was a Communist government created in Russia?**

**2. From what you have read, what do you think it means to live in a totalitarian state?**

**3. What was the reason for the “Great Terror”?**

**4. Why did Stalin’s Communist regime clash with Hitler’s Fascist regime?**

**5. Why did people follow Stalin and Hitler?  Give 3 reasons.**

**6. What was the Nazi-Soviet Pact? (p. 218 in text)**

**7. (from p.207 of the text)  Who was Mussolini and why was the government he created considered Fascist?**

**8. Who were the Blackshirts and what role did they play in Italy?  What methods did they use to control the population?**

**9. Why do you think Mussolini took such a great interest in educating the youth?**

**10. What actions did Mussolini take to increase the population?  Why did he want to do this?**

**11. (from p.209 of the text)  Describe the fascist movement in Canada.  Why do you think they had such strong support?**

**12.  How was Nazism spread in Canada and where was it most popular?**

**13.  What did Canadian fascist organizations hope to achieve and how did they try to achieve their goals?**

**14.  Why were some Canadians attracted to fascism?**

**15.  How did Anti-Semitism affect Jews in Canada?**

**16.  What were Canada’s policies towards Jewish immigration in the 1930s?  How did those policies compare to those of other nations?**

**17. What was the St. Louis?  How did Canadians react to its arrival off the coast of Canada?  What happened to the people on it?**