# Regina Riot Skit

**The Night Before** <br/>
P: We are going to reach Ottawa in the coming week eh?

J: Not quiet sure mate, the Bennett government doesn't seem too happy with what we are doing.

P: Hopefully by the the time we reach Ottawa, we will have enough men to show them that we should be heard.

J: Look, they're stopping the train!

P: Oh no, guess we are staying the night in Regina!

**The Next Morning** <br/>
P: Eh, where we going so early in the morning?

J: Heard we are going to be discussing our plans moving forward in the market square.

P: Wouldn't we be very exposed to possible attacks?



**Aftermath**
The Regina Riot was a `highly controversial part of Canadian history`. The J.B. Bennett government implemented `physical force on peaceful protestors` and resulted in the `death of a 
police officer`, and `injured many protestors`. The Regina Riot football team still acts as a solemn reminder to the horrible events that took place that day.