# Mackenzie King & Bennett

## William Lyon Mackenzie King
**William Lyon Mackenzie King** <br/>
Born in Berlin (Kitchener), Ontario and was the grandson of William Lyon Mackenzie, the leader of 1837 Rebellion in Upper Canada. He was also the mentee of Wilfred Laurier, thus he was really skilled as a compromiser.

**Beliefs:** <br/>
`National Unity` (French and English workers and bosses) <br/>
Move Canada from a `colony with a responsible government to an autonomous nation` within the British commonwealth <br/>
`Cautious politician` who tailored his policies to prevailing opinions

**Road to Independence:** <br/>
`1914-1919` Canada enters the first world war as a British colony <br/>
`1919` Paris peace conference - Canada attended with its own representative <br/>
`1922` Dardanelles Crisis - The Canadians told the British that they would not send troops to help them fight Turkey. <br/>
`1923` Halibut Treaty - Canada and U.S sign a treaty over the Halibut fishing rights in the Pacific (without Britain co-signing). The British protested but King stood firm. <br/>
`1925` King-Byng Crisis - King defies the authority of the Governor General. Hence, Governor General's position became symbolic and can no longer interfere with Canada's domestic affairs. <br/>
`1926` British Imperial Conference - The Balfour Report created at this conference stated that the colonies were equal to Britain, therefore they were able to make its own decisions and laws. <br/>
`1931` The statue of Westminster - formalised the Balfour decision and recognized Canada as a country able to make its own domestic and international decisions. <br/>
`1981` Canada creates its own constitution, severing the last tie that gave the British any opportunity to interfere with our politics. 

**Most Important Step:** <br/>
The King-Byng Crisis is one of most important steps to Canada's independance, as Mackenzie King defied the orders of Govenor General, starting to severe the tie between Canada and Great Britain. This showed that Canada no longer were under the authority of Great Britain and was capable of making their independant decisions.

**Personality:** <br/>
Lifelong `bachelor (He was never married)`, however he had few close female friends (Joan Patterson) <br/>
He was `afraid of public speaking` <br/>
`Kept a diary from his college years (1890s) till his death`. His will stated for the diary to be destroyed, but it was not. <br/>
The `diary revealed many unknown facts` about King (He communicated with spirits (Leonardo Da Vinci, Sir Wilfred Laurier, his dead mother, his dogs all of whom were named Pat, owned a Ouija Board and a Crystal Board). He was also racist against Jews and Japanese as mentioned on June 30th, 1937 in his diary.

**Interesting Facts:** <br/>
He once met `Adolf Hitler` who he described as `reasonable`, `caring`, and `one of the saviors of the worlds`

**1930 Election:** <br/>
King did not take the great depression seriously and made a careless remark that `he would not give 5 cents to the Provinces to help them deal with the depression` (Later stated that he was misquoted). His rival, conservative leader Richard Bedford Bennett promised that if he was elected, he would solve the problems of the depression.
**BENNETT WON THE ELECTION**


## Richard Bedford Bennett
**R.B. Bennett:**
Born on `July 3rd, 1870`, Hopewell Hill, New Brunswick <br/>
Was a `self-made millionare` <br/>

**Beliefs:** <br/>
Governments should `interfere as little as possible with the economy` <br/>
Good things happen to people when they work hard

**Personality:** <br/>
`Didn't drink alcohol`, but `ate candy` instead. <br/>
`Tough talker` in public, but `gentle` in private
`Conceited` (Did not listen to the opinions of others)
`Very intelligent` (Knew himself)

**Relief Camps:** <br/>
By the year 1932, the depression was worsening. Bennett was forced to use less traditional methods inorder provide a living. 
The government gave `$20 million` dollars to provinces to create relief programs. 
Relief payments were payments given to unemployed men if they could `prove that they had no job and no asset to sell` and were often not enough to live on. 
He also created `labour camps for single unemployed men`. 
They lived in bunkhouses and were paid `¢20 a day` minus the cost of food and a bed in return for `44 hours of weekly labour`. 

**On-to-Ottawa:** <br/>
In April, 1935, 1,500 men from B.C relief camps went on strike `Vancouver`, held deomstrations and `elicited support from other citizens`.
**Bennett ignored the protests**
Discouraged strike leaders moved the protests to Ottawa, and `one thousand strikers peacefully commandeered a freight train` and began the "on-to-ottawa" trek. 
They picked up more recurits along the way and by the time they reached Ottawa, their numbers had doubled to `2000`

**Regina Riot:** <br/>
In `Regina`, the federal government forbade the railways to take the men to Ottawa any further. Prime Minister Bennett finally agreed to meet the delagates of the rioters. 
The `meeting ended badly` as both sides exchanged violent language. On `July 1st`, rioters met in the `Regina Market Square` to discuss strategy when they were interrupted by 
`Mounties`. In less than 4 minutes, `1 policeman was dead, 40 protestors and 5 citizes were injured and 130 men were arrested.`

**Bennett's New Deal:** <br/>
After 4 years in office, and with the upcoming elections, he took radical action. He borrowed ideas from American President Franklin Rossevelt and change the plan to combat the depression. 
In a complete departure from his beliefs, he called for `Government control and regulation in Canada's social and economic affairs`. It changed the relationship between the government and the citizens.
Shifted Canada more to the left of the political spectrum.

**Parts of the New Deal:** <br/>
Start `circulating money by putting Canadians to work on infastructure projects` (such as rail building, electricity, etc), `improving job and guaranteeing loans given back to Canadian entrepreneurs. 
The Canadian government would also `protect the income of Canadians` with unemployment insurance, health insurance and closer regulations on working conditions.

**Bennett's Letters:** <br/>
The prime minsiter recieved `hundreds of letters from desperate Canadians` requesting help. Alone in his 17 room suite at Chateau Laurier Hotel, he worked through the night trying to keep up with the endless chorus of hearthbreak and dispear.
He would often end up `sending people money` because he felt helpless to do anything else.

**Bennett Leaves Canada:** <br/>
`Loses the 1935 election to Mackenzie King`. Bitter towards Canadian people, he `moves to England in 1938 never to reutrn`. He is the `only Canadian P.M not buried in Canada`.